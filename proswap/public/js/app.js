'use strict';

require('./bootstrap');

var _alpinejs = require('alpinejs');

var _alpinejs2 = _interopRequireDefault(_alpinejs);

var _focus = require('@alpinejs/focus');

var _focus2 = _interopRequireDefault(_focus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.Alpine = _alpinejs2.default;

_alpinejs2.default.plugin(_focus2.default);

_alpinejs2.default.start();