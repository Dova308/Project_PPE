<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShareController;
use App\Http\Controllers\SCPIController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', [DashboardController::class, "index"])->name('dashboard');

    Route::get('/product', [ProductController::class, "index"])->name('product');
    Route::get('/SCPI', [SCPIController::class, "index"])->name('scpi');
    Route::get('/share-wait', [ShareController::class, "show_wait"])->name('share-wait');
    Route::get('/showConfirmPurchase', [ShareController::class, "showConfirmPurchase"])->name('showConfirmPurchase');
    Route::get('/history', [ShareController::class, "history"])->name('history');

    Route::get('/products/{id}', [ShareController::class, 'getProduct'])->name('get-product');
    Route::get('/getScpiName/{id}', [ShareController::class, 'getScpiName'])->name('get-ScpiName');
    
    Route::post('/add-scpi', [SCPIController::class, "add"])->name('add-scpi');
    Route::post('/add-product', [ProductController::class, "add"])->name('add-product');

    Route::get('/buy-share/{share_id}', [ShareController::class, "index"])->name('buy-share');
    Route::post('/confirm-buy-share/{id}', [ShareController::class, "buy"])->name('confirm-buy-share');
    Route::post('/confirm-sold-share', [ShareController::class, "sold"])->name('confirm-sold-share');
    Route::post('/buyConfirmForm/{id}', [ShareController::class, "buyConfirmForm"])->name('buyConfirmForm');
    Route::get('/confirmPurchase/{id}', [ShareController::class, "confirmPurchase"])->name('confirmPurchase');
    Route::get('/refusePurchase/{id}', [ShareController::class, "refusePurchase"])->name('refusePurchase');
});
