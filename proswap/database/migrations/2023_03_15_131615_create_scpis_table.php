<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scpis', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->string('zone');
            $table->float('capital');
            $table->float('dividend_yield')->default(0);
            $table->float('net_asset_value')->default(0);
            $table->string('scpi_photo_path', 2048);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scpis');
    }
};
