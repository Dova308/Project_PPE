<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('scpi_id');
            $table->string('share_type');
            $table->string('status')->default('inactive');
            $table->bigInteger('available_shares');
            $table->bigInteger('share_price');
            $table->date('deadline');
            $table->string('building_photo_path', 2048)->nullable();
            $table->timestamps();
        
            $table->foreign('scpi_id')->references('id')->on('scpis');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shares');
    }
};
