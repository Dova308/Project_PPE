import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                //css
                'resources/css/app.css',
                'resources/css/auth/login.css',
                'resources/css/welcome.css',
                'resources/css/buy_share.css',
                //scss
                'resources/scss/auth/login.scss',
                'resources/scss/sidebar.scss',
                'resources/scss/dashboard.scss',
                'resources/scss/product.scss',
                'resources/scss/welcome.scss',
                //js
                'resources/js/app.js',
                'resources/js/auth/login.js',
                'resources/js/sidebar.js',
                'resources/js/dashboard.js',
                'resources/js/product.js',
                'resources/js/welcome.js',
                'resources/js/buy_share.js',
            ],
            refresh: [
                ...refreshPaths,
                'app/Http/Livewire/**',
            ],
        }),
    ],
});
