<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	  <title>{{ env('APP_NAME') }} Inscription</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/paper.js/0.12.11/paper-full.min.js"></script>

         <!--  style  -->
    @vite([
        'resources/scss/auth/login.scss',
        'resources/css/auth/login.css',
        

    //  <!-- Scripts -->
        'resources/js/auth/login.js'
    ])
</head>
<body>
	
  <div id="back">
    <div class="backLeft">
      <img src="{{ asset('images/login.gif') }}" alt="">
      <h3 class="typing">Bienvenue sur la page de connexion de ProSwap. Pour accéder à votre portefeuille immobilier et suivre vos investissements en temps réel, connectez-vous dès maintenant</h3>
    </div>
    <div class="backRight">
      <img src="{{ asset('images/register.gif') }}" alt="">
      <h3 class="typing">Vous êtes intéressé par l'investissement immobilier en SCPI ? Inscrivez-vous à ProSwap et accédez à des biens immobiliers de qualité, gérés par des professionnels.</h3>
    </div>
  </div>
  
  <div id="slideBox">
    <div class="topLayer">
      <div class="left">
        <div class="content">
          <h2>Inscription</h2>
          <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-element form-stack">
              <label for="username-signup" class="form-label">Prénom</label>
              <input id="ftname" type="text" name="ftname" :value="old('ftname')" required autocomplete="ftname" />
            </div>
            <div class="form-element form-stack">
              <label for="username-signup" class="form-label">Nom</label>
              <input id="ltname" type="text" name="ltname" :value="old('ltname')" required autocomplete="ltname" />
            </div>
            <div class="form-element form-stack">
              <label for="username-signup" class="form-label">Ville</label>
              <input id="city" type="text" name="city" :value="old('city')" required autocomplete="city" />
            </div>
            <div class="form-element form-stack">
              <label for="username-signup" class="form-label">Numéro de Téléphone</label>
              <input id="phone" type="text" name="phone">
            </div>
            <div class="form-element form-stack">
              <label for="email" class="form-label">Email</label>
              <input id="email" type="email" name="email" :value="old('email')" required />
            </div>
            <div class="form-element form-stack">
              <label for="password-signup" class="form-label">Mot de passe</label>
              <input id="password" type="password" name="password" required autocomplete="new-password" />
            </div>
            <div class="form-element form-stack">
              <label for="password-signup" class="form-label">Mot de passe confirmé</label>
              <input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>
            <div class="form-element form-checkbox">
              @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                   
                       
                      <input class="checkbox" type="checkbox" name="terms" id="terms"/>
                   
                      
                @endif
              <label for="confirm-terms">J'accepte<a href="{{ route('terms.show') }}"> les conditions d'utilisation</a> et <a href="{{ route('policy.show') }}">la politique de confidentialité</a></label>
            </div>
            <div class="form-element form-submit">
              <button id="signUp" class="signup" type="submit" name="signup">S'inscrire</button>
              <button id="goLeft" class="signup off">Déjà inscrit?</button> 
            </div>
          </form>
          <br>
          <x-validation-errors class="mb-4" />
        </div>
      </div>
      <div class="right">
        <div class="content">
          <h2>Connexion</h2>
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-element form-stack">
              <label for="username-login" class="form-label">Email</label>
              <input type="email" id="email" name="email" required autofocus autocomplete="email">
            </div>
            <div class="form-element form-stack">
              <label for="password-login" class="form-label">Mot de passe</label>
              <input type="password" id="password" name="password" required autocomplete="current-password">
            </div>
            <div class="form-element form-checkbox">
              <input id="remember_me" name="remember" type="checkbox"/>
              <label for="confirm-terms">se souvenir de moi</label>
            </div>
            <div class="form-element form-submit">
              <button id="logIn" class="login" type="submit" name="login">Connexion</button>
              <button id="goRight" class="login off" name="signup">Inscription</button>
            </div>
          </form>
          <br>
          <x-validation-errors class="mb-4" />
        </div>
      </div>
    </div>
  </div> 

</body>
</html>
