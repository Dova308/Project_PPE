<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} | Acheter des parts</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!--  style  -->
    @vite([
        'resources/css/buy_share.css',
            
    
    //  <!-- Scripts -->
        'resources/js/buy_share.js'
    ])
</head>
<body>
    <x-app-layout>
        
        <form id="form" method="POST" action="{{ route('confirm-buy-share', $share->id) }}" class="form-buy">
            @csrf
            <div class="form-header">
                <h1>Acheter des parts à {{ $share->scpi_name }}</h1>
            </div>
           
            <div class="form-step">
                <label for="shares">Nombre de parts à acheter</label>
                <input type="number" class="form-control" name="shares" id="shares" min="1" value="1" max="{{ $share->available_shares }}" required>
            </div>

            <button type="submit" class="btn btn-primary">Acheter des parts</button>
        </form>
            
    </x-app-layout>
</body>
</html>