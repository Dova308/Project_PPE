<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} | Acheter des parts</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!--  style  -->
    @vite([
        'resources/css/buy_share.css',
            
    
    //  <!-- Scripts -->
        'resources/js/buy_share.js'
    ])
</head>
<body>
    <x-app-layout>
        
        <form id="form" method="POST" action="{{ route('buyConfirmForm', ['id' => $id]) }}" class="form-buy">
            @csrf
            <div class="form-header">
                Formulaire de renseignement pour {{ $user->ftname }}
            </div>
            
            <div class="form-page active" id="page1">
                <div class="form-step">
                    <label for="full_name">Nom complet:</label>
                    <input type="text" value="{{ $user->ftname }} {{ $user->ltname }}" readonly required>
                </div>
                        
                <div class="form-step">
                    <label for="email">Email:</label>
                    <input type="text" id="email" name="email" value="{{ $user->email }}" readonly required>
                </div>

                <div class="form-step">
                    <label for="email">Numéro de téléphone:</label>
                    <input type="text" id="email" name="email" value="{{ $user->phone }}" readonly required>
                </div>

                <div class="form-step">
                    <label for="email">Ville:</label>
                    <input type="text" value="{{ $user->city }}" readonly required>
                </div>
                        
                <div class="form-nav">
                    <button type="button" id="next1">Suivant</button>
                </div>
            </div>
                    
            <div class="form-page" id="page2">
                <div class="form-step">
                    <label for="address">Adresse:</label>
                    <input type="text" id="address" name="address" placeholder="ex : 5 rue john doe" required>
                </div>
                        
                <div class="form-step">
                    <label for="postal_code">CP:</label>
                    <input type="text" id="postal_code" name="postal_code" placeholder="ex : 76000" required>
                </div>

                <div class="form-step">
                    <label for="country">Pays:</label>
                    <input type="text" id="country" name="country" placeholder="ex : France" required>
                </div>

                <div class="form-step">
                    <label for="nationality">Nationalité:</label>
                    <input type="text" id="nationality" name="nationality" placeholder="ex : française" required>
                </div>

                <div class="form-step">
                    <label for="date_of_birth">Date de naissance:</label>
                    <input type="date" id="date_of_birth" name="date_of_birth" required>
                </div>                

                <div class="form-step">
                    <label for="nationality">Genre:</label>
                    <select name="category" id="category" required>
                        <option value="M">Mr</option>
                        <option value="F">Mrs</option>
                    </select>
                </div>
                        
                <div class="form-nav">
                    <button type="button" id="prev2">Précédent</button>
                    <button type="button" id="next2">Suivant</button>
                </div>
            </div>
                    
            <div class="form-page" id="page3">
                <div class="form-step">
                    <p>
                        Félicitations ! Si vous confirmez les informations ci-dessous, 
                        vous deviendrez bientôt propriétaire d'une part de SCPI. 
                        Une fois votre demande confirmée, un PDF sécurisé sera généré, 
                        comportant une signature unique par technologie blockchain pour assurer 
                        la fiabilité et la sécurité de votre achat. Nous vous remercions de votre confiance
                        et n'hésitez pas à nous contacter si vous avez la moindre question.
                    </p>
                </div>
                
                <div class="form-nav">
                    <button type="button" id="prev3">Précédent</button>
                    <button type="submite">Confirmer</button>
                </div>
            </div>
        </form>

        <style>
            .form-nav button {
                margin-right: 30px;
            }
        </style>
            
    </x-app-layout>
</body>
</html>