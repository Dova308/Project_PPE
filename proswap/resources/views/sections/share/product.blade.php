<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} | Annonces</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <!--  style  -->
    @vite([
        'resources/scss/product.scss',
            
    
    //  <!-- Scripts -->
        'resources/js/product.js'
    ])
</head>
<body>
    <x-app-layout>
        <div class="app-container-product">
            <div class="list-content">
              <div class="list-content-header">
                <h1 class="list-content-headerText">Annonces des SCPI</h1>
                <div>
                  <button class="button-add" type="button" data-bs-toggle="modal" data-bs-target="#AddModal" data-bs-whatever="@getbootstrap">Ajouter une annonce</button>
                  <button class="button-add" type="button" data-bs-toggle="modal" data-bs-target="#SoldModal" data-bs-whatever="@getbootstrap">Vendre une/des part(s)</button>
                </div>
              </div>
              <div class="list-content-actions">
                <div class="search-wrapper">
                  <input class="search-input" id="mysearch" type="text" placeholder="Search">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="feather feather-search" viewBox="0 0 24 24">
                    <defs></defs>
                    <circle cx="11" cy="11" r="8"></circle>
                    <path d="M21 21l-4.35-4.35"></path>
                  </svg>
                </div>
                <div class="list-content-actions-wrapper">
                  <div class="filter-button-wrapper">
                    <button class="action-button filter jsFilter"><span>Filter</span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-filter"><polygon points="22 3 2 3 10 12.46 10 19 14 21 14 12.46 22 3"/></svg></button>
                    <div class="filter-menu">
                      <label>Category</label>
                      <select>
                        <option>All Categories</option>
                        <option>Furniture</option>                     
                        <option>Decoration</option>
                        <option>Kitchen</option>
                        <option>Bathroom</option>
                      </select>
                      <label>Status</label>
                      <select>
                        <option>All Status</option>
                        <option>Active</option>
                        <option>Disabled</option>
                      </select>
                      <div class="filter-menu-buttons">
                        <button class="filter-button reset">
                          Reset
                        </button>
                        <button class="filter-button apply">
                          Apply
                        </button>
                      </div>
                    </div>
                  </div>
                  <button class="action-button list active" title="List View">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list"><line x1="8" y1="6" x2="21" y2="6"/><line x1="8" y1="12" x2="21" y2="12"/><line x1="8" y1="18" x2="21" y2="18"/><line x1="3" y1="6" x2="3.01" y2="6"/><line x1="3" y1="12" x2="3.01" y2="12"/><line x1="3" y1="18" x2="3.01" y2="18"/></svg>
                  </button>
                  <button class="action-button grid" title="Grid View">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid"><rect x="3" y="3" width="7" height="7"/><rect x="14" y="3" width="7" height="7"/><rect x="14" y="14" width="7" height="7"/><rect x="3" y="14" width="7" height="7"/></svg>
                  </button>
                </div>
              </div>
              <div class="products-area-wrapper tableView">
                <div class="products-header">
                  <div class="product-cell image">
                    Nom
                    <button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button>
                  </div>
                  <div class="product-cell category">SCPI<button class="sort-button">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                  </button></div>
                  <div class="product-cell category">Type<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell status-cell">Status<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell sales">Part<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell stock">Deadline<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell price">Prix<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell price">Date de créaction<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                </div>
                @foreach ($product as $products)
                    <div class="products-row">
                        <button class="cell-more-button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical">
                                <circle cx="12" cy="12" r="1"/><circle cx="12" cy="5" r="1"/><circle cx="12" cy="19" r="1"/>
                            </svg>
                        </button>
                        <div class="product-cell image">
                            @if ($products->available_shares > 0)
                              @if ($user->date_of_birth != null)
                                <button type="button" data-bs-toggle="modal" data-bs-target="#BuyModal" data-bs-whatever="@getbootstrap" data-id="{{ $products->id }}">
                                  <img src="{{ $products->building_photo_path }}" alt="product">
                                </button>
                                <button type="button" data-bs-toggle="modal" data-bs-target="#BuyModal" data-bs-whatever="@getbootstrap" data-id="{{ $products->id }}">
                                  <span>{{ $products->name }}</span>
                                </button>
                              @else
                                <a href="{{ route('profile.show') }}">
                                  <img src="{{ $products->building_photo_path }}" alt="product">
                                </a>
                                <a href="{{ route('profile.show') }}">
                                  <span>{{ $products->name }}</span>
                                </a>
                              @endif
                            @else
                                <img src="{{ $products->building_photo_path }}" alt="product">
                                <span>{{ $products->name }}</span>
                            @endif
                        </div>
                        <div class="product-cell category"><span class="cell-label">SCPI:</span>{{ $products->scpi->name }}</div>
                        <div class="product-cell category"><span class="cell-label">Category:</span>{{ $products->share_type }}</div>
                        <div class="product-cell status-cell">
                            <span class="cell-label">Status:</span>
                            @if ($products->available_shares >= 2)
                                <span class="status active">Active</span>
                            @elseif ($products->available_shares == 1)
                                <span class="status active">En attente</span>
                            @else
                                <span class="status disabled">Inactive</span>
                            @endif
                        </div>
                        <div class="product-cell sales"><span class="cell-label">Part:</span>{{ $products->available_shares }}</div>
                        <div class="product-cell stock"><span class="cell-label">Deadline:</span>{{ $products->deadline }}</div>
                        <div class="product-cell price"><span class="cell-label">Prix:</span>{{ $products->share_price }} €</div>
                        <div class="product-cell price"><span class="cell-label">Date de création:</span>{{ $products->created_at }}</div>
                    </div>
                @endforeach
                
            </div>
          </div>

          <div class="modal fade" id="BuyModal" tabindex="-1" aria-labelledby="BuyModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title fs-5" id="BuyModalLabel">Acheter des parts à <span id="scpi-name"></span></h1>
                </div>
                <form id="buy-form" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    @csrf
                    <input type="hidden" name="product_id" id="product-id">
                    <div class="col-md-6">
                      <label for="shares" class="form-label">Nombre de parts à acheter</label>
                      <input type="number" class="form-control" name="shares" id="shares" min="1" value="1" required>
                      @error('shares')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="col-md-6">
                      <button class="button-add" type="submit">Acheter les parts</button>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="modal fade" id="AddModal" tabindex="-1" aria-labelledby="AddModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title fs-5" id="AddModalLabel">Ajouter un nouveau produit SCPI</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('add-product') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    @csrf
                      <div class="col-md-6">
                          <label for="scpi_id" class="form-label">SCPI</label>
                          <select class="form-control" @error('name') is-invalid @enderror" id="scpi_id" name="scpi_id" required>
                            @foreach ($scpis as $scpi)
                                <option value="{{ $scpi->id }}">{{ $scpi->name }}</option>
                            @endforeach
                          </select>
                          @error('scpi_id')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="name" class="form-label">Nom de l'annonce</label>
                        <input class="form-control @error('name') is-invalid @enderror" id="name" name="name" required value="{{ old('name') }}">
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="price" class="form-label">Prix d'une part</label>
                        <input class="form-control @error('price') is-invalid @enderror" id="price" name="share_price" required value="{{ old('share_price') }}">
                        @error('price')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="picture" class="form-label">Image</label>
                        <div class="file-upload">
                          <input type="file" id="picture" name="picture" required>
                          <label class="button" for="picture">Parcourir</label>
                        </div>
                        @error('picture')
                          <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>                      
                      <div class="col-md-6 deadline-style">
                        <label for="deadline" class="form-label">Deadline</label>
                        <input type="date" class="form-control @error('deadline') is-invalid @enderror" id="deadline" name="deadline" required value="{{ old('deadline') }}">
                        @error('deadline')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal">Close</button>
                    <button type="submit">Ajouter</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          
          <div class="modal fade" id="SoldModal" tabindex="-1" aria-labelledby="SoldModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title fs-5" id="SoldModalLabel">Vendre une/des part(s) ?</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('confirm-sold-share') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    @csrf
                      <div class="col-md-6">
                          <label for="scpi_id" class="form-label">Les parts appartenant à :</label>
                          <select class="form-control" @error('name') is-invalid @enderror" id="share_id" name="share_id" required>
                            @foreach ($shares as $share)
                                <option value="{{ $share['id'] }}">{{ $share['share'] }}</option>
                            @endforeach
                          </select>
                          @error('scpi_id')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="number_of_shares" class="form-label">Nombre de part a vendre </label>
                        <input class="form-control @error('number_of_shares') is-invalid @enderror" id="price" name="number_of_shares" required value="{{ old('number_of_shares') }}">
                        @error('number_of_shares')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal">Close</button>
                    <button type="submit">Confirmer</button>
                  </div>
                </form>
              </div>
            </div>
          </div>         

          <style>
              option, select {
                text-align: center;
              }

              .deadline-style input{
                display: block;
                width: 100%;
                padding: .375rem .75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: var(--bs-body-color);
                background-color: var(--bs-form-control-bg);
                background-clip: padding-box;
                border: var(--bs-border-width) solid var(--bs-border-color);
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: .375rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
              }
              
              .modal-body {
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                text-align: center
              }

              .modal-body > div {
                margin: 10px;
              }

              .file-upload {
                position: relative;
                overflow: hidden;
                margin-top: 10px;
              }

              .file-upload input[type='file'] {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                padding: 0;
                font-size: 20px;
                cursor: pointer;
                opacity: 0;
                filter: alpha(opacity=0);
              }

              .file-upload .button {
                display: inline-block;
                background-color: #6c757d;
                color: #fff;
                padding: 8px 20px;
                font-size: 16px;
                font-weight: bold;
                border-radius: 5px;
                transition: all 0.2s ease;
              }

              .file-upload .button:hover {
                background-color: #5a6268;
              }

              input[type="date"] {
                text-align: center;
              }

          </style>
          <script>
            $('#BuyModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('id');
                var modal = $(this);
            
                // Récupérer le nom de la SCPI correspondante et afficher dans le modal
                $.get('/getScpiName/' + id, function (data) {
                    modal.find('.modal-title').text('Acheter des parts à ' + data);
                });
            
                // Récupérer les autres informations de la part correspondante et les afficher dans le modal
                $.get('/products/' + id, function (data) {
                    modal.find('form').attr('action', '/confirm-buy-share/' + data.id);
                    modal.find('#shares').attr('max', data.available_shares);
                });
            });
            </script>            
    </x-app-layout>
</body>
</html>