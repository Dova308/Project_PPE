<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} | Annonces en attente</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <!--  style  -->
    @vite([
        'resources/scss/product.scss',
            
    
    //  <!-- Scripts -->
        'resources/js/product.js'
    ])
</head>
<body>
    <x-app-layout>
        <div class="app-container-product">
            <div class="list-content">
              <div class="list-content-header">
                <h1 class="list-content-headerText">Vos annonce en attente</h1>
              </div>
              <div class="list-content-actions">
                <div class="search-wrapper">
                  <input class="search-input" type="text" placeholder="Search">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="feather feather-search" viewBox="0 0 24 24">
                    <defs></defs>
                    <circle cx="11" cy="11" r="8"></circle>
                    <path d="M21 21l-4.35-4.35"></path>
                  </svg>
                </div>
              </div>
              <div class="products-area-wrapper tableView">
                <div class="products-header">
                  <div class="product-cell image">
                    Nom SCPI
                    <button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button>
                  </div>
                  <div class="product-cell status-cell">Prix<button class="sort-button">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                  </button></div>
                  <div class="product-cell status-cell">Part(s)<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell status-cell">Status<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                  <div class="product-cell status-cell">Date d'achat<button class="sort-button">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path fill="currentColor" d="M496.1 138.3L375.7 17.9c-7.9-7.9-20.6-7.9-28.5 0L226.9 138.3c-7.9 7.9-7.9 20.6 0 28.5 7.9 7.9 20.6 7.9 28.5 0l85.7-85.7v352.8c0 11.3 9.1 20.4 20.4 20.4 11.3 0 20.4-9.1 20.4-20.4V81.1l85.7 85.7c7.9 7.9 20.6 7.9 28.5 0 7.9-7.8 7.9-20.6 0-28.5zM287.1 347.2c-7.9-7.9-20.6-7.9-28.5 0l-85.7 85.7V80.1c0-11.3-9.1-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v352.8l-85.7-85.7c-7.9-7.9-20.6-7.9-28.5 0-7.9 7.9-7.9 20.6 0 28.5l120.4 120.4c7.9 7.9 20.6 7.9 28.5 0l120.4-120.4c7.8-7.9 7.8-20.7-.1-28.5z"/></svg>
                    </button></div>
                </div>
                @foreach ($tracking as $track)
                
                    <div class="products-row">
                      <button class="cell-more-button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"/><circle cx="12" cy="5" r="1"/><circle cx="12" cy="19" r="1"/></svg>
                      </button>
                        <div class="product-cell image">
                          
                          <span>{{ $track->transaction->share->scpi->name }}</span>
                          
                        </div>
                      <div class="product-cell status-cell">
                        <span class="cell-label">Prix:</span>
                            <span>{{ $track->transaction['transaction_amount'] }}</span>
                      </div>
                      <div class="product-cell status-cell">
                        <span class="cell-label">Parts:</span>
                            <span>{{ $track->transaction['number_of_shares'] }}</span>
                      </div>
                      <div class="product-cell status-cell">
                        <span class="cell-label">Status:</span>
                            <span class="status active">{{ $track['transaction_status'] }}</span>
                      </div>
                      <div class="product-cell status-cell">
                        <span class="cell-label">Date d'achat:</span>
                            <span>{{ $track->transaction['created_at'] }}</span>
                      </div>
                    </div>
                @endforeach
            </div>
          </div>

          <div class="modal fade" id="AddModal" tabindex="-1" aria-labelledby="AddModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title fs-5" id="AddModalLabel">Ajouter un nouveau produit SCPI</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('add-product') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    @csrf
                      <div class="col-md-6">
                          <label for="name" class="form-label">Nom scpi</label>
                          <input class="form-control @error('name') is-invalid @enderror" id="name" name="scpi_name" required value="{{ old('scpi_name') }}">
                          @error('name')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                      <div class="col-md-6">
                          <label for="type" class="form-label">Type</label>
                          <input class="form-control @error('type') is-invalid @enderror" id="type" name="share_type" required value="{{ old('share_type') }}">
                          @error('type')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                      <div class="col-md-6">
                          <label for="parts" class="form-label">Parts</label>
                          <input class="form-control @error('parts') is-invalid @enderror" id="parts" name="available_shares" required value="{{ old('available_shares') }}">
                          @error('parts')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="price" class="form-label">Prix</label>
                        <input class="form-control @error('price') is-invalid @enderror" id="price" name="share_price" required value="{{ old('share_price') }}">
                        @error('price')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="col-md-6">
                        <label for="picture" class="form-label">Image</label>
                        <div class="file-upload">
                          <input type="file" id="picture" name="picture" required>
                          <label class="button" for="picture">Parcourir</label>
                        </div>
                        @error('picture')
                          <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>                      
                      <div class="col-md-6 deadline-style">
                        <label for="deadline" class="form-label">Deadline</label>
                        <input type="date" class="form-control @error('deadline') is-invalid @enderror" id="deadline" name="deadline" required value="{{ old('deadline') }}">
                        @error('deadline')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" data-bs-dismiss="modal">Close</button>
                    <button type="submit">Ajouter</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <style>
              .deadline-style input{
                display: block;
                width: 100%;
                padding: .375rem .75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: var(--bs-body-color);
                background-color: var(--bs-form-control-bg);
                background-clip: padding-box;
                border: var(--bs-border-width) solid var(--bs-border-color);
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border-radius: .375rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
              }
              
              .modal-body {
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                text-align: center
              }

              .modal-body > div {
                margin: 10px;
              }

              .file-upload {
                position: relative;
                overflow: hidden;
                margin-top: 10px;
              }

              .file-upload input[type='file'] {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                padding: 0;
                font-size: 20px;
                cursor: pointer;
                opacity: 0;
                filter: alpha(opacity=0);
              }

              .file-upload .button {
                display: inline-block;
                background-color: #6c757d;
                color: #fff;
                padding: 8px 20px;
                font-size: 16px;
                font-weight: bold;
                border-radius: 5px;
                transition: all 0.2s ease;
              }

              .file-upload .button:hover {
                background-color: #5a6268;
              }

              input[type="date"] {
                text-align: center;
              }

          </style>

    </x-app-layout>
</body>
</html>