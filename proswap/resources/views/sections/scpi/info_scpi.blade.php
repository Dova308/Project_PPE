<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} | </title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <!--  style  -->
    @vite([
        'resources/scss/product.scss',
            
    
    //  <!-- Scripts -->
        'resources/js/product.js'
    ])
</head>
<body>
    <x-app-layout>
        <div class="app-container-product">
            <div class="list-content">
              <div class="list-content-header">
                <h1 class="list-content-headerText">Name SCPI</h1>
              </div>
              <div class="list-content-actions">
                <div class="search-wrapper">
                  <input class="search-input" type="text" placeholder="Search">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="feather feather-search" viewBox="0 0 24 24">
                    <defs></defs>
                    <circle cx="11" cy="11" r="8"></circle>
                    <path d="M21 21l-4.35-4.35"></path>
                  </svg>
                </div>
                <div class="list-content-actions-wrapper">
                  
                </div>
              </div>
              <div class="products-area-wrapper tableView">
                <div class="products-header">
                  
                </div>
                

            </div>
          </div>

    </x-app-layout>
</body>
</html>