<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ProSwap') }}</title>

        <!-- Fonts -->
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <!--  style  -->
        @vite([
            'resources/scss/sidebar.scss',
                
        
        //  <!-- Scripts -->
            'resources/js/sidebar.js'
        ])
    </head>
    <body>

        <div class="app-container">
            <div class="app-header">
                <div class="app-header-left">
                    
                    <p class="app-name">ProSwap</p>
                </div>
                <div class="app-header-right">
                    <p class="app-name">{{ Auth::user()->balance }} €</p>
                    <button class="profile-btn">
                        <img class="h-10 w-10 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->ftname }}" />
                    <span>{{ Auth::user()->ftname }} {{ Auth::user()->ltname }}</span>
                    </button>
                </div>
            </div>
            <div class="app-content">
                @livewire('navigation-menu')
            
                {{ $slot }}
        </div>

        @stack('modals')

        @livewireScripts
    </div>
    </body>
</html>
