<x-guest-layout>
    <div class="pt-4 bg-gray-100">
        <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0">
            <div class="w-full sm:max-w-2xl mt-6 p-6 bg-white shadow-md overflow-hidden sm:rounded-lg prose">
                <h1>Politique de sécurité</h1>
                <p>
                    1.	Identification et authentification
                    Les utilisateurs doivent s'identifier en utilisant une adresse e-mail et un mot de passe fort.
                    Les mots de passe doivent être stockés de manière sécurisée en utilisant un hachage fort et un sel unique pour chaque utilisateur.
                    Les utilisateurs doivent être encouragés à utiliser une authentification à deux facteurs pour renforcer leur sécurité.
                </p>
                <p>
                    2.	Gestion des accès
                    Les utilisateurs doivent avoir des niveaux d'accès appropriés en fonction de leur rôle et de leurs responsabilités.
                    Les droits d'accès doivent être revus régulièrement pour s'assurer qu'ils sont toujours appropriés et mis à jour en conséquence.
                    Les anciens comptes d'utilisateurs doivent être désactivés immédiatement lorsqu'un utilisateur quitte l'entreprise ou n'a plus besoin d'accéder au système.
                </p>
                <p>
                    3.	Gestion des vulnérabilités
                    Les mises à jour de sécurité pour le système d'exploitation, les logiciels et les applications doivent être appliquées régulièrement pour prévenir les vulnérabilités.
                    Les vulnérabilités connues doivent être suivies et corrigées dès que possible.
                    Les tests de pénétration doivent être effectués régulièrement pour identifier les vulnérabilités et les corriger avant qu'elles ne soient exploitées.
                </p>
                <p>
                    4.	Surveillance et gestion des incidents
                    Les activités du système et les logs doivent être surveillés régulièrement pour détecter les activités suspectes ou malveillantes.
                    Les incidents de sécurité doivent être signalés immédiatement à l'administrateur système pour une réponse rapide.
                    Les plans d'intervention en cas d'incident doivent être en place et régulièrement mis à jour pour s'assurer qu'ils sont efficaces.
                </p>
                <p>
                    5.	Sécurité physique
                    Le serveur doit être hébergé dans un centre de données sécurisé avec des mesures de sécurité physiques appropriées.
                    Les sauvegardes doivent être stockées dans un emplacement sûr et hors site pour assurer la récupération des données en cas de perte ou de catastrophe.
                </p>
                <p>
                    6.	Politique de confidentialité
                    Les données des utilisateurs doivent être collectées et stockées conformément à la politique de confidentialité.
                    Les utilisateurs doivent être informés de la façon dont leurs données seront utilisées et protégées.
                    Les politiques de confidentialité doivent être régulièrement révisées pour s'assurer qu'elles sont toujours conformes aux réglementations applicables.
                </p>
            </div>
        </div>
    </div>
</x-guest-layout>
