<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proswap</title>

        <!-- Fonts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        


        <!--  style  -->
        @vite([
            'resources/css/welcome.css',
                
        
        //  <!-- Scripts -->
            'resources/js/welcome.js'
        ])
        
    </head>
    
    <body>
    
        
        <!-- Start Landing Page -->

        <div class="landing-page">
            <div class="container">
            <div class="header-area">
                <div class="logo">Pro<b>Swap</b></div>
                <ul class="links">        
                        @if (Route::has('login'))
                        @auth
                            <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                        @else
                            <li><a href="{{ route('login') }}" style="color: black">Connexion</a></li>

                            @if (Route::has('register'))
                                <li><a href="{{ route('register') }}">Inscription</a></li>
                            @endif

                        @endauth
                                
                        @endif
                    
                </ul>
            </div>
            <hr>
            <div class="info">
                <h2>Vous cherchez à diversifier votre portefeuille immobilier ? Cédez vos parts SCPI en toute simplicité avec Proswap!</h2>
                <p>Entreprise agrée par l'état, depuis 2023.</p>
                <button>Plus d'Informations</button>
            </div>
            <div class="image">
                <img src="https://cdn-icons-png.flaticon.com/512/4350/4350196.png">
            </div>
            <div class="clearfix"></div>
            </div>
        </div>
        
    </body>
</html>
