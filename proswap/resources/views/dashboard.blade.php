<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }} Dashboard</title>

    <!--  style  -->
    @vite([
        'resources/scss/dashboard.scss',
            
    
    //  <!-- Scripts -->
        'resources/js/dashboard.js'
    ])
</head>
<body>
    <x-app-layout>
        
        <div class="projects-section">
            <div class="projects-section-header">
              <p>Dashboard</p>
              <p class="time">{{ \Carbon\Carbon::now()->locale('fr_FR')->isoFormat('dddd D MMMM YYYY') }}</p>
            </div>
            <div class="projects-section-line">
                <div class="projects-status">
                    <div class="item-status">
                    <span class="status-number">{{ $user->parts_total }}</span>
                    <span class="status-type">Parts détenues</span>
                </div>
                <div class="item-status">
                    <span class="status-number">{{ $user->parts_buy }}</span>
                    <span class="status-type">Parts acheter</span>
                </div>
                <div class="item-status">
                    <span class="status-number">{{ $user->parts_sold }}</span>
                    <span class="status-type">Parts vendu</span>
                </div>
            </div>
            <div class="view-actions">
                <button class="view-btn list-view" title="List View">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list">
                    <line x1="8" y1="6" x2="21" y2="6" />
                    <line x1="8" y1="12" x2="21" y2="12" />
                    <line x1="8" y1="18" x2="21" y2="18" />
                    <line x1="3" y1="6" x2="3.01" y2="6" />
                    <line x1="3" y1="12" x2="3.01" y2="12" />
                    <line x1="3" y1="18" x2="3.01" y2="18" /></svg>
                </button>
                <button class="view-btn grid-view active" title="Grid View">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid">
                    <rect x="3" y="3" width="7" height="7" />
                    <rect x="14" y="3" width="7" height="7" />
                    <rect x="14" y="14" width="7" height="7" />
                    <rect x="3" y="14" width="7" height="7" /></svg>
                </button>
              </div>
            </div>
            <div class="project-boxes jsGridView">
                @foreach ($shares as $share)
                    <div class="project-box-wrapper">
                        <div class="project-box" style="background-color: #d5deff;">
                        <div class="project-box-content-header">
                            <p class="box-content-header">{{ $share['share'] }}</p>
                            <p class="box-content-subheader">{{ $share['scpi'] }}</p>
                        </div>
                        <div class="box-progress-wrapper">
                            <p class="box-progress-header">Progress</p>
                            <div class="box-progress-bar">
                            <span class="box-progress" style="width: {{ $share['percentage'] }}%; background-color: #4067f9"></span>
                            </div>
                            <p class="box-progress-percentage">{{ $share['percentage'] }}%</p>
                        </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="messages-section">
            <button class="messages-close">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle">
                <circle cx="12" cy="12" r="10" />
                <line x1="15" y1="9" x2="9" y2="15" />
                <line x1="9" y1="9" x2="15" y2="15" /></svg>
            </button>
            <div class="projects-section-header">
            <p>Nombre d'annonces en portefeuille</p>
            </div>
            <div class="messages">
                @foreach ($shares as $share)
                    <div class="message-box">
                        <img src="{{ $share['photo'] }}" alt="profile image">
                        <div class="message-content">
                            <div class="message-header">
                                <div class="name"><span>Nom : {{ $share['share'] }}</span></div>
                                <br>
                            </div>
                            <br>
                            <p>
                            <span>Mes parts : {{ $share['total_parts'] }}</span>  
                            </p>
                            <br>
                            <p>
                                <span>Dernier achat : {{ $share['last_buy'] }}</span>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>        

    </x-app-layout>
</body>
</html>
