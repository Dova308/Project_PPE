<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Facture d'achat</title>
    <style>
        /* Style pour le PDF */
        body {
            font-family: sans-serif;
            font-size: 14px;
            line-height: 1.5;
            color: #333;
            background-color: #fff;
        }
        table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
        }
        table th {
            padding: 5px;
            font-weight: bold;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #333;
            border-bottom: 1px solid #333;
        }
        table td {
            padding: 5px;
            vertical-align: top;
            border-bottom: 1px solid #ccc;
        }
        .text-right {
            text-align: right;
        }
    </style>
</head>
<body>
    <div id="header">
        <h1>Facture de vente</h1>
        <p>Date : {{ $transaction->created_at->format('d/m/Y') }}</p>
    </div>
    <div id="content">
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Type</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $transaction->share->scpi->name }}</td>
                    <td>{{ $transaction->share->scpi->type }}</td>
                    <td>{{ $transaction->number_of_shares }}</td>
                    <td>{{ number_format($transaction->transaction_amount / $transaction->number_of_shares, 2, ',', ' ') }} €</td>
                    <td>{{ number_format($transaction->transaction_amount, 2, ',', ' ') }} €</td>
                </tr>
            </tbody>
        </table>
        <p>Vous avez vendu {{ $transaction->number_of_shares }} parts de la SCPI : {{ $transaction->share->scpi->name }}.</p>
    </div>
</body>
</html>
