<x-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Profile Information') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Update your account\'s profile information and email address.') }}
    </x-slot>

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview" style="display: none;">
                    <span class="block rounded-full w-20 h-20 bg-cover bg-no-repeat bg-center"
                          x-bind:style="'background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-secondary-button>
                @endif

                <x-input-error for="photo" class="mt-2" />
            </div>
        @endif

        <!-- Prénom -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="ftname" value="{{ __('Prénom') }}" />
            <x-input id="ftname" type="text" class="mt-1 block w-full" wire:model.defer="state.ftname" autocomplete="ftname" />
            <x-input-error for="ftname" class="mt-2" />
        </div>

        <!-- Nom -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="ltname" value="{{ __('Nom') }}" />
            <x-input id="ltname" type="text" class="mt-1 block w-full" wire:model.defer="state.ltname" autocomplete="ltname" />
            <x-input-error for="ltname" class="mt-2" />
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="email" value="{{ __('Email') }}" />
            <x-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" autocomplete="username" />
            <x-input-error for="email" class="mt-2" />

            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::emailVerification()) && ! $this->user->hasVerifiedEmail())
                <p class="text-sm mt-2">
                    {{ __('Your email address is unverified.') }}

                    <button type="button" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" wire:click.prevent="sendEmailVerification">
                        {{ __('Click here to re-send the verification email.') }}
                    </button>
                </p>

                @if ($this->verificationLinkSent)
                    <p v-show="verificationLinkSent" class="mt-2 font-medium text-sm text-green-600">
                        {{ __('A new verification link has been sent to your email address.') }}
                    </p>
                @endif
            @endif
        </div>

        <!-- Wallet -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="balance" value="{{ __('Wallet') }}" />
            <x-input id="balance" type="text" class="mt-1 block w-full" wire:model.defer="state.balance" autocomplete="balance" />
            <x-input-error for="balance" class="mt-2" />
        </div>

        <!-- Phone -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="phone" value="{{ __('Phone') }}" />
            <x-input id="phone" type="text" class="mt-1 block w-full" wire:model.defer="state.phone" autocomplete="phone" />
            <x-input-error for="phone" class="mt-2" />
        </div>

        <!-- Ville -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="city" value="{{ __('Ville') }}" />
            <x-input id="city" type="text" class="mt-1 block w-full" wire:model.defer="state.city" autocomplete="city" />
            <x-input-error for="city" class="mt-2" />
        </div>

        <!-- address -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="address" value="{{ __('Adresse') }}" />
            <x-input id="address" type="text" class="mt-1 block w-full" wire:model.defer="state.address" autocomplete="address" />
            <x-input-error for="address" class="mt-2" />
        </div>

        <!-- postal code -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="postal_code" value="{{ __('Code Postal') }}" />
            <x-input id="postal_code" type="text" class="mt-1 block w-full" wire:model.defer="state.postal_code" autocomplete="postal_code" />
            <x-input-error for="postal_code" class="mt-2" />
        </div>

        <!-- country -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="country" value="{{ __('Pays') }}" />
            <x-input id="country" type="text" class="mt-1 block w-full" wire:model.defer="state.country" autocomplete="country" />
            <x-input-error for="country" class="mt-2" />
        </div>

        <!-- nationality -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="nationality" value="{{ __('Nationalitée') }}" />
            <x-input id="nationality" type="text" class="mt-1 block w-full" wire:model.defer="state.nationality" autocomplete="nationality" />
            <x-input-error for="nationality" class="mt-2" />
        </div>

        <!-- date of birth -->
        <div class="col-span-6 sm:col-span-4">
            <x-label for="date_of_birth" value="{{ __('Date de naissance') }}" />
            <x-input id="date_of_birth" type="date" class="mt-1 block w-full" wire:model.defer="state.date_of_birth" autocomplete="date_of_birth" />
            <x-input-error for="date_of_birth" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-action-message>

        <x-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Save') }}
        </x-button>
    </x-slot>
</x-form-section>
