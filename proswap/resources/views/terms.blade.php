<x-guest-layout>
    <div class="pt-4 bg-gray-100">
        <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0">
            <div class="w-full sm:max-w-2xl mt-6 p-6 bg-white shadow-md overflow-hidden sm:rounded-lg prose">
                <h1>Condition d'utilisation</h1>
                <p>
                    1.	Acceptation des conditions d'utilisation
                    En utilisant le site web ProSwap, vous acceptez les présentes conditions d'utilisation et vous vous engagez à les respecter. Si vous n'êtes pas d'accord avec ces conditions, veuillez ne pas utiliser ce site.
                </p>
                <p>
                    2.	 Accès au site
                    Le site est accessible gratuitement depuis n’importe où par tout utilisateur disposant d’un accès à Internet. Tous les frais nécessaires pour l’accès aux services (matériel informatique, connexion Internet…) sont à la charge de l’utilisateur.
                    L’accès aux services dédiés aux membres s’effectue à l’aide d’un identifiant et d’un mot de passe.
                    Pour des raisons de maintenance ou autres, l’accès au site peut être interrompu ou suspendu par l’éditeur sans préavis ni justification.
                </p>
                <p>
                    3.	Propriété intellectuelle
                    Tous les contenus, tels que les textes, les images et les vidéos, présents sur le site web ProSwap sont la propriété exclusive de ProSwap ou de ses partenaires et sont protégés par les lois sur les droits d'auteur. Vous ne pouvez pas utiliser, copier ou distribuer ces contenus sans l'autorisation préalable de ProSwap.
                </p>
                <p>
                    4.	Confidentialité
                    ProSwap respecte la vie privée de ses utilisateurs et collecte et utilise les données personnelles conformément à sa politique de confidentialité. En utilisant le site web ProSwap, vous acceptez la collecte et l'utilisation de vos données personnelles conformément à cette politique.
                </p>
                <p>
                    5.	 Cookies
                    Lors des visites sur le site, l’installation automatique d’un cookie sur le logiciel de navigation de l’Utilisateur peut survenir.
                    Les cookies correspondent à de petits fichiers déposés temporairement sur le disque dur de l’ordinateur de l’Utilisateur. Ces cookies sont nécessaires pour assurer l’accessibilité et la navigation sur le site. Ces fichiers ne comportent pas d’informations personnelles et ne peuvent pas être utilisés pour l’identification d’une personne.
                    L’information présente dans les cookies est utilisée pour améliorer les performances de navigation sur le site ProSwap.
                    En naviguant sur le site, l’Utilisateur accepte les cookies. Leur désactivation peut s’effectuer via les paramètres du logiciel de navigation.
                </p>
                <p>
                    6.	Droit applicable et juridiction compétente
                    Le présent contrat est soumis à la législation française. L’absence de résolution à l’amiable des cas de litige entre les parties implique le recours aux tribunaux français compétents pour régler le contentieux.
                </p>
                <p>
                    7.	Modifications des conditions d'utilisation
                    ProSwap se réserve le droit de modifier les présentes conditions d'utilisation à tout moment et sans préavis. Nous vous invitons à consulter régulièrement cette page pour être informé des éventuelles modifications.
                </p>

                <p>
                    En utilisant le site web ProSwap, vous acceptez toutes les conditions d'utilisation présentées ci-dessus. Si vous avez des questions ou des préoccupations concernant ces conditions, veuillez nous contacter.
                </p>
            </div>
        </div>
    </div>
</x-guest-layout>
