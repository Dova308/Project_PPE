// Récupère les pages du formulaire
const formPages = document.querySelectorAll(".form-page");

// Récupère les boutons de navigation
const prevButtons = document.querySelectorAll(".form-nav button[id^='prev']");
const nextButtons = document.querySelectorAll(".form-nav button[id^='next']");

// Parcourt chaque bouton de navigation
prevButtons.forEach((button) => {
  button.addEventListener("click", () => {
    // Récupère la page active
    const activePage = document.querySelector(".form-page.active");

    // Récupère la page précédente
    const prevPage = activePage.previousElementSibling;

    // Met à jour les classes des pages
    activePage.classList.remove("active");
    prevPage.classList.add("active");
  });
});

nextButtons.forEach((button) => {
  button.addEventListener("click", () => {
    // Récupère la page active
    const activePage = document.querySelector(".form-page.active");

    // Récupère la page suivante
    const nextPage = activePage.nextElementSibling;

    // Met à jour les classes des pages
    activePage.classList.remove("active");
    nextPage.classList.add("active");
  });
});

// Empêche l'envoi du formulaire lors de l'appui sur la touche Enter
const form = document.getElementById("form");
form.addEventListener("keypress", (event) => {
  if (event.key === "Enter") {
    event.preventDefault();
  }
});
