<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;

    // Dans le modèle Share
    public function scpi()
    {
        return $this->belongsTo(Scpi::class);
    }

    protected $fillable = [
        'id',
        'scpi_name',
        'share_type',
        'status',
        'available_shares',
        'share_price',
        'deadline',
        'building_photo_path'
    ];
}
