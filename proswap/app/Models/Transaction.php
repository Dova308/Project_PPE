<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'scpi_name',
        'share_type',
        'available_shares',
        'share_price',
        'deadline',
        'building_photo_path'
    ];

    public function share()
    {
        return $this->belongsTo(Share::class);
    }
}
