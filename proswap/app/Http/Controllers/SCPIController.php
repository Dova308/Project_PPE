<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Scpi;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class SCPIController extends Controller
{
    public function index() {
        $scpi = Scpi::orderBy('name', 'asc')->get();

        return view('sections.scpi.list_scpi', compact('scpi'));
    }

    public function add(Request $request)
    {
        // Valider les données envoyées par le formulaire
        $validatedData = $request->validate([
            'name' => 'required',
            'type' => 'required',
            'zone' => 'required',
            'capital' => 'required',
            'picture' => 'required|image|mimes:jpeg,png,jpg,gif|max:5000',
        ]);

        // Télécharger l'image
        $imageName = time().'.'.$request->picture->extension();
        $image = Image::make($request->picture)->resize(1024, 768)->encode();
        Storage::put('public/img-scpi/' . $imageName, $image); 

        // Créer un nouvel objet Share avec les données du formulaire
        $scpi = new Scpi();
        $scpi->name = $validatedData['name'];
        $scpi->type = $validatedData['type'];
        $scpi->zone = $validatedData['zone'];
        $scpi->capital = $validatedData['capital'];
        $scpi->scpi_photo_path = 'storage/img-scpi/'.$imageName;

        // Enregistrer l'objet Share dans la base de données
        $scpi->save();

        // Rediriger l'utilisateur vers la page de liste des produits avec un message de confirmation
        return redirect()->route('scpi')->with('success', 'La scpi a été ajouté avec succès.');
    }
}
