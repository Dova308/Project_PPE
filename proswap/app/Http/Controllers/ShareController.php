<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Share;
use App\Models\Transaction;
use App\Models\Tracking;
use App\Models\Securitie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Dompdf\Dompdf;

class ShareController extends Controller
{
    public function getProduct($id)
    {
        $product = Share::findOrFail($id);

        return response()->json($product);
    }

    public function getScpiName($id) 
    {
        $share = Share::findOrFail($id);
        return $share->scpi->name;
    }

    public function buy(Request $request, $id)
    {
        // Récupérer l'utilisateur actuellement connecté
        $buyer = auth()->user();

        // Récupérer l'objet Share correspondant à l'ID donné
        $share = Share::findOrFail($id);
        $scpiName = $share->scpi->name;

        // Vérifier que le nombre de parts demandées est inférieur ou égal au nombre de parts disponibles
        $number_of_shares = (int) $request->input('shares', 1); // par défaut 1 part
        if ($number_of_shares > $share->available_shares) {
            return response()->json(['error' => 'Insufficient shares available'], 422);
            die();
        }

        // Calculer le montant total de la transaction en fonction du nombre de parts achetées
        $share_price = bcmul($share->share_price, 1, 2); // limiter à 2 décimales pour éviter les erreurs d'arrondi
        $transaction_amount = bcmul($number_of_shares, $share_price, 2);

        // Vérifier que l'utilisateur a suffisamment de fonds pour acheter toutes les parts
        if ($buyer->balance < $transaction_amount) {
            return response()->json(['error' => 'Insufficient funds'], 422);
            die();
        }

        // Créer une nouvelle transaction avec les détails de l'achat
        $transaction = new Transaction;
        $transaction->user_id = $buyer->id;
        $transaction->share_id = $share->id;
        $transaction->transaction_amount = $transaction_amount;
        $transaction->number_of_shares = $number_of_shares;
        $transaction->type = 'Achat';
        $transaction->transaction_date = now();
        $transaction->save();

        // Ajouter une entrée de suivi pour la transaction
        $tracking = new Tracking;
        $tracking->transaction_id = $transaction->id;
        $tracking->user_id = $buyer->id;
        $tracking->transaction_status = 'En attente';
        $tracking->save();

        // Ajouter la part achetée à la liste des titres de l'utilisateur
        $securitie = new Securitie;
        $securitie->user_id = $buyer->id;
        $securitie->name = $scpiName;
        $securitie->value = $transaction_amount;
        $securitie->save();

        // Mettre à jour le nombre de parts disponibles pour l'offre de SCPI
        $share->available_shares -= $request->shares;
        $share->save();

        return $this->show_wait();
        //return response()->json(['error' => $request->shares], 422);
    }

    public function sold(Request $request)
    {
        // Valider les données envoyées par le formulaire
        $validatedData = $request->validate([
            'share_id' => 'required',
            'number_of_shares' => 'required'
        ]);

        // Récupérer l'utilisateur actuellement connecté
        $sold = auth()->user();

        // Récupérer l'objet Share correspondant à l'ID donné
        $share = Share::findOrFail($validatedData['share_id']);
        $scpiName = $share->scpi->name;

        // Calculer le montant total de la transaction en fonction du nombre de parts achetées
        $share_price = bcmul($share->share_price, 1, 2); // limiter à 2 décimales pour éviter les erreurs d'arrondi
        $transaction_amount = bcmul($validatedData['number_of_shares'], $share_price, 2);


        $sold->balance = $sold->balance + $transaction_amount;
        $sold->save();

        // Créer une nouvelle transaction avec les détails de l'achat
        $transaction = new Transaction;
        $transaction->user_id = $sold->id;
        $transaction->share_id = $share->id;
        $transaction->transaction_amount = $transaction_amount;
        $transaction->number_of_shares = $validatedData['number_of_shares'];
        $transaction->type = 'Vente';
        $transaction->transaction_date = now();
        $transaction->save();

        // Ajouter une entrée de suivi pour la transaction
        $tracking = new Tracking;
        $tracking->transaction_id = $transaction->id;
        $tracking->user_id = $sold->id;
        $tracking->transaction_status = 'Confirmée';
        $tracking->save();

        // Ajouter la part achetée à la liste des titres de l'utilisateur
        $securitie = new Securitie;
        $securitie->user_id = $sold->id;
        $securitie->name = $scpiName;
        $securitie->value = $share_price;
        $securitie->save();
        
        // Mettre à jour le nombre de parts disponibles pour l'offre de SCPI
        $share->available_shares += $validatedData['number_of_shares'];
        $share->save();

        // Nombre total de part vendu
        $parts_total_sold = $validatedData['number_of_shares'] + $sold->parts_sold;
        $parts_total = $sold->parts_total - $validatedData['number_of_shares'];
        $sold->parts_sold = $parts_total_sold;
        $sold->parts_total = $parts_total;
        $sold->save();

        // Instancier une nouvelle instance de Dompdf
        $dompdf = new Dompdf();

        // Générer le contenu HTML du PDF
        $html = view('pdf.sold', compact('transaction', 'sold'))->render();

        // Ajouter le contenu HTML au Dompdf
        $dompdf->loadHtml($html);

        // Configurer les options du Dompdf
        $dompdf->setPaper('A4', 'portrait');

        // Générer le PDF
        $dompdf->render();

        $date = date('Y-m-d H:i:s');

        // Enregistrer le PDF sur le disque
        $pdf_content = $dompdf->output();
        Storage::disk('public')->put("transactions/sold/{$transaction->share->scpi->name}{$date}.pdf", $pdf_content);

        return redirect()->route('product')->with('success', 'Le produit a été ajouté avec succès.');
        //return response()->json(['error' => $id], 422);
    }

    public function history()
    {
        $user = auth()->user();

        $tracking = Tracking::where('user_id', $user->id)
        ->with(['transaction.share.scpi'])
        ->get();

        return view('sections.share.history', compact('tracking'));
    }

    public function show_wait()
    {
        $tracking = Tracking::where('transaction_status', 'En attente')
        ->with(['transaction.share.scpi'])
        ->get();
        
        $user = auth()->user();

        return view('sections.share.panier-wait', compact('tracking'));
    }

    public function showConfirmPurchase()
    {
        $tracking = Tracking::where('transaction_status', 'En attente')
                            ->with(['transaction.share.scpi'])
                            ->get();
        $user = auth()->user();

        return view('sections.share.confirm-status-share', compact('tracking'));
    }


    public function confirmPurchase($id)
    {
        $user = auth()->user();
        
        // Mettre à jour le suivi de la transaction
        $tracking = Tracking::findOrFail($id);
        $tracking->transaction_status = 'Confirmée';
        $tracking->save();

        // Récupérer l'id de la transaction du solde de l'acheteur
        $transaction = Transaction::findOrFail($tracking->transaction_id);

        // Nombre total de part aquis
        $parts_total = $transaction->number_of_shares + $user->parts_total;
        $user->parts_total = $parts_total;

        // Nombre total de part acheter
        $parts_total_buy = $transaction->number_of_shares + $user->parts_buy;
        $user->parts_buy = $parts_total_buy;

        // Déduire le montant exact de la transaction du solde de l'acheteur
        $user->balance = bcsub($user->balance, $transaction->transaction_amount, 2);
        $user->save();

        // Instancier une nouvelle instance de Dompdf
        $dompdf = new Dompdf();

        // Générer le contenu HTML du PDF
        $html = view('pdf.buy', compact('transaction', 'user'))->render();

        // Ajouter le contenu HTML au Dompdf
        $dompdf->loadHtml($html);

        // Configurer les options du Dompdf
        $dompdf->setPaper('A4', 'portrait');

        // Générer le PDF
        $dompdf->render();

        $date = date('Y-m-d H:i:s');

        // Enregistrer le PDF sur le disque
        $pdf_content = $dompdf->output();
        Storage::disk('public')->put("transactions/buy/{$transaction->share->scpi->name}{$date}.pdf", $pdf_content);

        // Rediriger l'admin vers la page showConfirmPurchase
        return $this->showConfirmPurchase();
    }

    public function RefusePurchase($id)
    {
        // Mettre à jour le suivi de la transaction
        $tracking = Tracking::findOrFail($id);
        $tracking->transaction_status = 'Refusé';
        $tracking->save();

        // Récupérer l'id de la transaction du solde de l'acheteur
        $transaction = Transaction::findOrFail($tracking->transaction_id);

        // Mettre à jour le nombre de parts disponibles pour l'offre de SCPI
        $share = Share::findOrFail($transaction->share_id);
        $share->available_shares += $transaction->number_of_shares;
        $share->save();

        $tracking->
        $transaction->delete();

        // Rediriger l'admin vers la page showConfirmPurchase
        return $this->showConfirmPurchase();
    }

}
