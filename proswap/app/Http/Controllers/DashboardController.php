<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tracking;
use App\Models\Transaction;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        // Récupérer toutes les transactions confirmées de l'utilisateur à partir de la table Tracking
        $transactions = Tracking::where('user_id', $user->id)
            ->where('transaction_status', 'Confirmée')
            ->whereHas('transaction', function ($query) {
                $query->where('type', 'Achat');
            })
            ->with('transaction.share')
            ->get();

        // Initialiser un tableau pour stocker les annonces de SCPI avec le nombre total de parts achetées
        $shares = [];

        // Pour chaque transaction, ajouter le nombre de parts achetées à l'annonce correspondante dans le tableau
        foreach ($transactions as $tracking) {
            // Vérifier que $tracking->transaction est bien un objet Transaction
            if ($tracking->transaction instanceof Transaction) {
                $transaction = $tracking->transaction;
                $annonce_id = $transaction->share_id;
                if (!isset($shares[$annonce_id])) {
                    $shares[$annonce_id] = [
                        'share' => $transaction->share->name,
                        'scpi' => $transaction->share->scpi->name,
                        'zone' => $transaction->share->scpi->zone,
                        'photo' => $transaction->share->building_photo_path,
                        'last_buy' => $transaction->created_at,
                        'total_parts' => $transaction->number_of_shares,
                        'percentage' => $transaction->number_of_shares / 100 * 100, // Calcule le pourcentage de parts achetées par rapport à 100 parts (maximum pour tous les shares)
                    ];
                } else {
                    $shares[$annonce_id]['total_parts'] += $transaction->number_of_shares;
                    $shares[$annonce_id]['percentage'] = $shares[$annonce_id]['total_parts'] / 100 * 100; // Recalcule le pourcentage de parts achetées pour cet share
                }
            }
        }

        return view('dashboard', ['shares' => $shares, 'user' => $user]);
        //return response()->json(['error' => $shares], 422);
    }


}
