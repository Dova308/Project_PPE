<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Share;
use App\Models\Transaction;
use App\Models\Scpi;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        
        // Lister les Share de la base de données
        $products = Share::with('scpi')
                //->orderBy('deadline', 'asc')
                ->get();

        // Récupérer toutes les transactions confirmées de l'utilisateur
        $transactions = Transaction::where('user_id', $user->id)
                ->get();
        
        // Initialiser un tableau pour stocker les annonces de SCPI avec le nombre total de parts achetées
        $shares = [];

        // Pour chaque transaction, ajouter le nombre de parts achetées à l'annonce correspondante dans le tableau
        foreach ($transactions as $transaction) {
            $annonce_id = $transaction->share_id;
            if (!isset($shares[$annonce_id])) {
                $shares[$annonce_id] = [
                'id' => $transaction->share->id,    
                'share' => $transaction->share->name,
                ];
            }
        }

        $scpis = Scpi::all();

        return view('sections.share.product', ['product' => $products, 'scpis' => $scpis, 'user' => $user, 'shares' => $shares]);
    }

    public function add(Request $request)
    {
        // Valider les données envoyées par le formulaire
        $validatedData = $request->validate([
            'scpi_id' => 'required',
            'name' => 'required',
            'deadline' => 'required|date',
            'share_price' => 'required',
            'picture' => 'required|image|mimes:jpeg,png,jpg,gif|max:5000',
        ]);

        $scpis = Scpi::findOrFail($validatedData['scpi_id']);

        // Télécharger l'image
        $imageName = time().'.'.$request->picture->extension();
        $image = Image::make($request->picture)->resize(1024, 768)->encode();
        Storage::put('public/img-product/' . $imageName, $image); 

        $available_shares = 100;

        // Créer un nouvel objet Share avec les données du formulaire
        $share = new Share();
        $share->scpi_id = $validatedData['scpi_id'];
        $share->name = $validatedData['name'];
        $share->share_type = $scpis->type;
        if ($available_shares > 0) 
        {
            $share->status = 'Active';
        }
        else
        {
            $share->status = 'Inactive';
        }
        $share->available_shares = $available_shares;
        $share->deadline = $validatedData['deadline'];
        $share->share_price = $validatedData['share_price'];
        $share->building_photo_path = 'storage/img-product/'.$imageName;

        // Enregistrer l'objet Share dans la base de données
        $share->save();

        // Rediriger l'utilisateur vers la page de liste des produits avec un message de confirmation
        return redirect()->route('product')->with('success', 'Le produit a été ajouté avec succès.');
    }

}
